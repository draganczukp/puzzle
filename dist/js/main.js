"use strict";
var State;
(function (State) {
    State["INACTIVE"] = "inactive";
    State["ACTIVE"] = "active";
    State["SELECTABLE"] = "selectable";
    State["GOAL"] = "goal";
})(State || (State = {}));
const gridStates = [];
const getNeighbours = (idx) => {
    const neighbours = [];
    // UP
    if (idx > 5)
        neighbours.push(idx - 5);
    // DOWN
    if (idx < 20)
        neighbours.push(idx + 5);
    // LEFT
    if (idx !== 0 && idx !== 5 && idx !== 10 && idx !== 15 && idx !== 20)
        neighbours.push(idx - 1);
    // RIGHT
    if (idx !== 4 && idx !== 9 && idx !== 14 && idx !== 19 && idx !== 24)
        neighbours.push(idx + 1);
    return neighbours;
};
const sync = () => {
    gridStates.forEach(gridState => {
        const elt = gridState.element;
        const state = gridState.state;
        elt.dataset['state'] = state;
    });
};
const reset = () => {
    gridStates.forEach(gridState => {
        gridState.state = State.GOAL;
    });
    gridStates[22].state = State.SELECTABLE;
    for (let idx of [0, 5, 18, 20, 21, 23]) {
        gridStates[idx].state = State.INACTIVE;
    }
    sync();
};
(() => {
    const elts = document.querySelectorAll('.puzzle');
    elts.forEach((elt, idx) => {
        const element = elt;
        gridStates.push({
            index: idx,
            element,
            state: State.INACTIVE
        });
        sync();
        element.addEventListener('click', () => {
            const state = gridStates.find(s => s.index === idx);
            if (state.state === State.SELECTABLE) {
                state.state = State.ACTIVE;
                gridStates.filter(s => s.state === State.SELECTABLE)
                    .forEach(s => s.state = State.GOAL);
                const neighbours = getNeighbours(state.index);
                console.log(neighbours);
                neighbours.forEach(i => {
                    const neighbour = gridStates.find(s => s.index === i);
                    if (neighbour.state === State.GOAL) {
                        neighbour.state = State.SELECTABLE;
                    }
                });
            }
            sync();
        });
    });
    const resetButton = document.querySelector('#reset-button');
    resetButton.addEventListener('click', () => reset());
    reset();
})();
//# sourceMappingURL=main.js.map